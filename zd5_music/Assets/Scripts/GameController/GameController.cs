﻿using System.Collections.Generic;
using UnityEngine;

namespace SDA.Breakout2D
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] 
        private Paddle paddle;
        public Paddle Paddle => paddle;

        [SerializeField] 
        private Ball ball;
        public Ball Ball => ball;

        private InputSystem inputSystem;
        public InputSystem InputSystem => inputSystem;

        [SerializeField]
        private MainMenu mainMenu;
        public MainMenu MainMenu => mainMenu;

        [SerializeField]
        private GameView gameView;
        public GameView GameView => gameView;

        [SerializeField]
        private PointsSystem pointsSystem;
        public PointsSystem PointsSystem => pointsSystem;

        [SerializeField]
        private MusicSystem musicSystem;
        public MusicSystem MusicSystem => musicSystem;

        [SerializeField]
        private BricksManager bricksManager;
        public BricksManager BricksManager => bricksManager;
        
        private IState currentState;

        private void Awake()
        {
            inputSystem = new InputSystem();
        }
        
        private void Start()
        {
            bricksManager.Initialize(pointsSystem, musicSystem);
            ball.Initailize(musicSystem);
            paddle.Initialize(musicSystem);
            ChangeState(new GameState());
        }

        private void Update()
        {
            currentState.Tick(this);
        }

        private void FixedUpdate()
        {
            currentState.FixedTick(this);
        }

        public void ChangeState(IState newState)
        {
            currentState?.Dispose(this);
            currentState = newState;
            currentState.Init(this);
        }
    }
}