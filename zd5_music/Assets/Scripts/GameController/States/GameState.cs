namespace SDA.Breakout2D
{
    public class GameState : IState
    {
        public void Init(GameController gC)
        {
            gC.InputSystem.EnableSystem();
            gC.Ball.Respawn();
            gC.Paddle.ShowMouth();
            gC.GameView.ShowView();
        }

        public void Tick(GameController gC)
        {
            gC.InputSystem.UpdateInput();
        }

        public void FixedTick(GameController gC)
        {
            gC.Paddle.MovePaddle(gC.InputSystem.HorizontalInput);
        }

        public void Dispose(GameController gC)
        {
            gC.InputSystem.DisableSystem();
            gC.GameView.HideView();
        }
    }
}