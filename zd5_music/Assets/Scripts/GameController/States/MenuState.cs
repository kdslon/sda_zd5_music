namespace SDA.Breakout2D
{
    public class MenuState : IState
    {
        public void Init(GameController gC)
        {
            gC.MainMenu.ShowView();
        }

        public void Tick(GameController gC)
        {
            
        }

        public void FixedTick(GameController gC)
        {
            
        }

        public void Dispose(GameController gC)
        {
            gC.MainMenu.HideView();
        }
    }
}