namespace SDA.Breakout2D
{
    public interface IState
    {
        public void Init(GameController gC);
        public void Tick(GameController gC);
        public void FixedTick(GameController gC);
        public void Dispose(GameController gC);
    }
}