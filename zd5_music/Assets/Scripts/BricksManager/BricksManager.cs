using System.Collections.Generic;
using UnityEngine;

namespace SDA.Breakout2D
{
    public class BricksManager : MonoBehaviour
    {
        [SerializeField]
        private List<Brick> bricks;

        public void Initialize(PointsSystem pS, MusicSystem mS)
        {
            foreach (var brick in bricks)
            {
                brick.Initalize(pS, mS);
            }
        }
    }
}