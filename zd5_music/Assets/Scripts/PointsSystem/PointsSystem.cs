using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Breakout2D
{
    public class PointsSystem : MonoBehaviour
    {
        private int points;

        public void AddPoint()
        {
            points++;
        }
    }
}