﻿using DG.Tweening;
using UnityEngine;

namespace SDA.Breakout2D
{
    public class Paddle : MonoBehaviour
    {
        [SerializeField] private float paddleSpeed = 5f;

        private Rigidbody2D paddleRb;

        [SerializeField] 
        private Transform upMouth;
        [SerializeField] 
        private Transform downMouth;

        [SerializeField]
        private MusicSystem musicSystem;

        public bool IsSmiling { get; private set; }

        private Vector2 startMouthScale;
        
        private void Awake()
        {
            paddleRb = GetComponent<Rigidbody2D>();
            startMouthScale = upMouth.localScale;
        }

        public void Initialize(MusicSystem mS)
        {
            this.musicSystem = mS;
        }

        public void MovePaddle(float direction)
        {
            paddleRb.velocity = new Vector2(direction * paddleSpeed, 0f);
        }

        public void ShowMouth()
        {
            upMouth.transform.localScale = Vector3.zero;
            upMouth.gameObject.SetActive(true);
            upMouth.transform.DOScale(startMouthScale, .5f);
        }

        public void TweenToSmile()
        {
            IsSmiling = true;
            downMouth.gameObject.SetActive(false);
            upMouth.transform.localScale = Vector3.zero;
            upMouth.gameObject.SetActive(true);
            upMouth.transform.DOScale(startMouthScale, .5f);
        }

        public void TweenToScared()
        {
            IsSmiling = false;
            upMouth.gameObject.SetActive(false);
            downMouth.transform.localScale = Vector3.zero;
            downMouth.gameObject.SetActive(true);
            downMouth.transform.DOScale(startMouthScale, .5f);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.collider.CompareTag("Ball"))
            {
                if(!IsSmiling)
                    TweenToSmile();
                var hitPoint = other.contacts[0].point;
                var paddleCenter = (Vector2) transform.position;
                var diff = (hitPoint - paddleCenter).normalized;
                other.collider.GetComponent<Ball>().ChangeDirection(diff.x);
                musicSystem.PlaySound(SoundId.BallWall);
            }
        }
    }

}