﻿using UnityEngine;

namespace SDA.Breakout2D
{
    public class Roof : MonoBehaviour
    {
        [SerializeField] private Paddle paddle;
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if(other.CompareTag("Ball") && paddle.IsSmiling)
                paddle.TweenToScared();
        }
    }
}