﻿using DG.Tweening;
using UnityEngine;

namespace SDA.Breakout2D
{
    public class Brick : MonoBehaviour
    {
        private Collider2D collider;
        private Vector3 startScale;

        private bool isHit;
        private PointsSystem pointsSystem;
        private MusicSystem musicSystem;
        
        private void Awake()
        {
            collider = GetComponent<BoxCollider2D>();
        }

        public void Initalize(PointsSystem pS, MusicSystem musicSystem)
        {
            startScale = transform.localScale;
            this.pointsSystem = pS;
            this.musicSystem = musicSystem;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.collider.CompareTag("Ball"))
            {
                isHit = true;
                var delaySeq = DOTween.Sequence();
                delaySeq.SetDelay(.1f).Append(transform.DOScale(Vector3.zero, .2f));
                collider.enabled = false;
                pointsSystem.AddPoint();
                musicSystem.PlaySound(SoundId.Pling);
            }
        }
    }
}