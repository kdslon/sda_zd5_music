﻿using System;
using DG.Tweening;
using UnityEngine;

namespace SDA.Breakout2D
{
    public class Wall : MonoBehaviour
    {
        private Vector3 startScale;
        
        private void Start()
        {
            startScale = transform.localScale;
        }
        
        public void ScaleUpAndDown()
        {
            var scaleSeq = DOTween.Sequence();
            scaleSeq
                .Append(transform.DOScale(startScale + Vector3.one * .2f, .1f))
                .Append(transform.DOScale(startScale, .1f));
        }
    }
}
