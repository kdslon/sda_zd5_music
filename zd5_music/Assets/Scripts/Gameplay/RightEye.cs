﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Breakout2D
{
    public class RightEye : MonoBehaviour
    {
        [SerializeField] private Transform eyePivot;
        [SerializeField] private Transform leftEyePivot;

        public void Update()
        {
            eyePivot.rotation = leftEyePivot.transform.rotation;
        }
    }
}