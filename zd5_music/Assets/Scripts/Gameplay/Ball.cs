﻿using System;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SDA.Breakout2D
{
    public class Ball : MonoBehaviour
    {
        [SerializeField] private Transform spawnPoint;
        [SerializeField] private float startSpeed = 5f;
        
        private Rigidbody2D ballRb;
        private SpriteRenderer spriteRenderer;
        private readonly int[] possibleDirections = {-1, 1};
        private Vector3 startScale;
        private Color startColor;
        private Camera mainCam;
        private MusicSystem musicSystem;

        private Action onCollision;
        private bool isShaking = false;
        
        
        private void Awake()
        {
            ballRb = GetComponent<Rigidbody2D>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            startScale = transform.localScale;
            startColor = spriteRenderer.color;
            mainCam = Camera.main;
        }

        public void Initailize(MusicSystem mS)
        {
            this.musicSystem = mS;
        }

        public void OnCollision_AddListener(Action callback)
        {
            onCollision += callback;
        }

        public void OnCollision_RemoveListener(Action callback)
        {
            onCollision -= callback;
        }

        public void Respawn()
        {
            transform.position = spawnPoint.position;
            var direction = new Vector2(possibleDirections[Random.Range(0, 2)], 1f);
            ballRb.velocity = direction * startSpeed;
            ChangeBallRotation();
        }

        public void ChangeDirection(float dirValue)
        {
            var direction = new Vector2(dirValue, 1f);
            ballRb.velocity = direction * startSpeed;
        }

        private void ScaleUpAndDown()
        {
            var scaleSeq = DOTween.Sequence();
            scaleSeq
                .Append(transform.DOScale(startScale + Vector3.one * .3f, .1f))
                .Append(transform.DOScale(startScale, .1f));
        }
        
        private void ChangeColorAndBack()
        {
            var colorSeq = DOTween.Sequence();
            colorSeq
                .Append(spriteRenderer.DOColor(Color.white, .1f))
                .Append(spriteRenderer.DOColor(startColor, .1f));
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            ScaleUpAndDown();
            ChangeColorAndBack();
            onCollision?.Invoke();
            
            //Uncomment if you want to see camera shake effect :)
            // if (!isShaking)
            // {
            //     isShaking = true;
            //     mainCam.DOShakePosition(.2f, Vector3.one * .1f).OnComplete(()=>isShaking = false);
            // }


            if (other.collider.CompareTag("Wall"))
            {
                other.collider.GetComponent<Wall>().ScaleUpAndDown();
                musicSystem.PlaySound(SoundId.BallWall);
            }
            ChangeBallRotation();
        }

        private void ChangeBallRotation()
        {
            var velocity = ballRb.velocity;
            if (velocity.x < 0 && velocity.y < 0)
            {
                transform.DORotate(new Vector3(0f,0f,150f), .1f);
            }
            else if(velocity.x < 0 && velocity.y > 0)
            {
                transform.DORotate(new Vector3(0f,0f,45f), .1f);
            }
            else if(velocity.x > 0 && velocity.y > 0)
            {
                transform.DORotate(new Vector3(0f,0f,-45f), .1f);
            }
            else if(velocity.x > 0 && velocity.y < 0)
            {
                transform.DORotate(new Vector3(0f,0f,150f), .1f);
            }
        }
    }
}