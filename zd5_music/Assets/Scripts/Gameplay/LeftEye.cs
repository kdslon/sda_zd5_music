﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SDA.Breakout2D
{
    public class LeftEye : MonoBehaviour
    {
        [SerializeField] private Transform eyePivot;
        [SerializeField] private Ball ball;

        public void Update()
        {
            Vector3 diff = ball.transform.position - transform.position;
            diff.Normalize();
 
            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            eyePivot.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }
    }
}