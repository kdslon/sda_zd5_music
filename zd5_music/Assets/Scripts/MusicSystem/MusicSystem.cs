using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

namespace SDA.Breakout2D
{
    public enum SoundId
    {
        BallWall,
        Pling // = 1
    }

    public class MusicSystem : MonoBehaviour
    {
        //DRY -> Don't repeat yourself
        
        [SerializeField]
        private List<AudioClip> clips;

        [SerializeField]
        private List<AudioClip> plings;

        [SerializeField]
        private List<AudioSource> sources;

        [SerializeField]
        private AudioMixer audioMixer;

        //Przykładowy kod do ustawiania głośności na mixerze. Pamiętajcie o wykorzystaniu logarytmu
        //x należy do przedziału [0.0001; 1]
        // private void Start()
        // {
        //     SetVolume();
        // }
        //
        // public void SetVolume()
        // {
        //     var volume = Mathf.Log10(.5f) * 20f;
        //     audioMixer.SetFloat("Master", volume);
        // }

        public void PlaySound(SoundId id)
        {
            var intId = (int)id;
            switch (id)
            {
                case SoundId.Pling:
                    PlayRandomSound(id);
                    break;
                case SoundId.BallWall:
                    PlaySound(intId);
                    break;
            }
        }

        public void PlaySound(int id)
        {
            AudioSource freeSource = null;
            bool isAnySourceFree = TryGetFreeSource(out freeSource);
            
            if(!isAnySourceFree)
                return;
            
            //var = typ przypisany do zmiennej, w tym przypadku AudioClip
            var clip = clips[id];
            
            //1 sposób
            // source.clip = clip;
            // source.Play();

            //2 sposób
            freeSource.PlayOneShot(clip);
        }

        private void PlayRandomSound(SoundId id)
        {
            AudioSource freeSource = null;
            bool isAnySourceFree = TryGetFreeSource(out freeSource);
            
            if (id == SoundId.Pling)
            {
                var randomId = Random.Range(0, plings.Count);
                if(isAnySourceFree)
                    freeSource.PlayOneShot(plings[randomId]);
            }
        }

        public void RandomExample()
        {
            int randomInt = Random.Range(0, 5); //losuje od 1 do 4
            float randomFloat = Random.Range(1f, 5f); //losuje od 1 do 5 i wszystko pomiędzy 1 i 5
        }

        private bool TryGetFreeSource(out AudioSource finalSource)
        {
            foreach (var source in sources)
            {
                if(source.isPlaying)
                    continue;

                finalSource = source;
                return true;
            }

            finalSource = null;
            return false;
        }
    }
}