﻿using UnityEngine;

namespace SDA.Breakout2D
{
    public class InputSystem
    {
        public float HorizontalInput { get; private set; }
        private bool isSystemEnabled;

        public void EnableSystem()
        {
            isSystemEnabled = true;
        }

        public void DisableSystem()
        {
            isSystemEnabled = false;
        }

        public void UpdateInput()
        {
            if (!isSystemEnabled)
                return;
            
            HorizontalInput = Input.GetAxisRaw("Horizontal");
        }
    }
}